import java.util.*;

class Student implements Comparable<Student> {
  private String fullName;
  private Integer age;

  public Student(String fullName, Integer age) {
    this.fullName = fullName;
    this.age = age;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  @Override
  public String toString() {
    return "Student{" + "fullName='" + fullName + '\'' + ", age=" + age + '}' + '\n';
  }

  @Override
  public int compareTo(Student student) {
    int compareFullName = fullName.compareTo(student.getFullName());
    if (compareFullName != 0) return compareFullName;
    return age - student.getAge();
  }
}
